from darkflow.net.build import TFNet
import matplotlib.pyplot as plt
import cv2
import numpy as np
import math
import time
import itertools as it
from collections import defaultdict
from pyardrone import ARDrone
import logging
import datetime

date = datetime.datetime.today()
test_video_name = "tx1_exp1_" + str(date) +".avi"

options = {"model": 'cfg/tiny-yolo-voc-custom.cfg', 
           "load": 67025, 
           #"threshold": 0.2, 
           "gpu": .8
          }

tfnet = TFNet(options)

def closer(distance, area):
    a = False
    b = False
    if distance[2] > distance[1] > distance[0]:
        a = True
    if area[2] < area[1] < area[0]:
        b = True
    return a and b
actual_width = {"Parrot Bebop Drone" : 14, "DJI Mavic Air Drone" : 9.5, "AR Parrot Drone" : 20}
centers = defaultdict(list) 
areas = defaultdict(list)
distances = defaultdict(list)
lockout = True 
alpha0 = .944 / 360
K = np.array([[640, 0, 320], [0, 640, 180], [0, 0, 1]])
ki = np.linalg.inv(K)
r1 = ki.dot([320, 180, 1.0])
def boxing(original_img , predictions, client):
    global centers
    global lockout
    newImage = np.copy(original_img)
    imgcenter = (int(newImage.shape[1]/2), int (newImage.shape[0]/2))
    newImage = cv2.circle(newImage, (imgcenter[0], imgcenter[1]), 5, (255, 0, 0), thickness = 1)
    for result in predictions:
        top_x = result['topleft']['x']
        top_y = result['topleft']['y']
        btm_x = result['bottomright']['x']
        btm_y = result['bottomright']['y']
        confidence = result['confidence']
        width = btm_x - top_x
        height = btm_y - top_y
        known_width = actual_width[result['label']] 
        dist_from_cam = round((known_width*564) / width, 3)
        width_prop = width / dist_from_cam
        if confidence > 0.3:
            found_label = False
            newImage = cv2.rectangle(newImage, (top_x, top_y), (btm_x, btm_y), (255,0,0), 3)
            centerx = int((top_x+btm_x)/2)
            centery = int((top_y+btm_y)/2)
            centerxy = [centerx, centery]
            centerdistance = math.sqrt((imgcenter[0]-centerx)**2 + (imgcenter[1]-centery)**2)
            r2 = ki.dot([centerx, centery, 1.0])
            centers[result['label']].append(centerxy)
            distances[result['label']].append(dist_from_cam)
            areas[result['label']].append(round(height*width, 3))
            angle2 = r1.dot(r2) / (np.linalg.norm(r1) * np.linalg.norm(r2))
            angle2 = math.degrees(np.arccos(angle2))
            label = result['label'] + " " + str(round(dist_from_cam/12,3)) + "ft " + str(round(angle2, 3)) + "deg "
            result['coords'] = (round(centerdistance, 3), round(angle2,3))
            result['time'] = int(time.time())
            manuever_time = .5
            manuever_speed = .5
            if dist_from_cam < 72 and lockout:
                if centerx < imgcenter[0]:
                    print("MOVE LEFT ", label)
                    start_time = int(time.time())
                    f.write("MOVE LEFT: " + label + '\n') 
                    while int(time.time() - start_time) < manuever_time:
                        client.move(left = manuever_speed)
                    lockout = False
                elif centerx > imgcenter[0]:
                    print("MOVE RIGHT ", label)
                    f.write("MOVE RIGHT: " + label) 
                    start_time = int(time.time()) 
                    while int(time.time() - start_time) < manuever_time:
                        client.move(right = manuever_speed)
                    lockout = False
            elif len(centers[result['label']]) > 2:
                last_3 = centers[result['label']][-3:]
                lastdistance_3 = distances[result['label']][-3:]
                lastarea_3 = areas[result['label']][-3:]
                if closer(lastdistance_3, lastarea_3):
                    if centerx < imgcenter[0]:
                        print("CLOSER LEFT " + label)
                        f.write("ClOSER LEFT: " + label + '\n')
                        start_time = int(time.time()) 
                        while int(time.time() - start_time) < manuever_time:
                            client.move(left= manuever_speed)
                        lockout = False
                    elif centerx > imgcenter[0]:
                        print("CLOSER RIGHT ", label)
                        f.write("CLOSER RIGHT: " + label + '\n')
                        start_time = int(time.time()) 
                        while int(time.time() - start_time) < manuever_time:
                            client.move(right = manuever_speed)
                        lockout = False
            newImage = cv2.circle(newImage, (centerx, centery), 5, (255,0,0), thickness = -1)
            newImage = cv2.line(newImage, (centerx, centery), (imgcenter[0], imgcenter[1]), (0,255,0), thickness = 1)
            newImage = cv2.putText(newImage, label, (top_x, top_y-5), cv2.FONT_HERSHEY_COMPLEX_SMALL , 0.8, (0, 230, 0), 1, cv2.LINE_AA)
            f.write("Predicted: " + label + "\n")
    return newImage

f = open("tx1_exp1_" + str(datetime.datetime.today()) + ".txt", "w")
fourcc = cv2.VideoWriter_fourcc(*'DIVX')
out = cv2.VideoWriter(test_video_name,fourcc, 20.0, (640,360))
# logging.basicConfig(level=logging.DEBUG)
client = ARDrone()
client.navdata_ready.wait()  # wait until NavData is ready
client.video_ready.wait()
initial_time = int(time.time())
time_to_fly = 30 
while not client.state.fly_mask:
    client.takeoff()
time.sleep(5)
try:
    while (int(time.time()) - initial_time <= time_to_fly):
        frame = np.asarray(client.frame)
        results = tfnet.return_predict(frame)
        new_frame = boxing(frame, results, client)
        cv2.imshow('frame', new_frame)
        out.write(new_frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
finally:
    while client.state.fly_mask:
        client.land()
    client.close()
    f.close()
print(datetime.datetime.today())