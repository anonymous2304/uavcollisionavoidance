import json
import os
# for json_file in os.listdir('.'):
#   json_data = json.loads(open(json_file).read())
#   for item in json_data:
#       if item['label'] == 'AR Parrot Drone':
#           item['label'] == 'AR_Parrot_Drone'
#       elif item['label'] == 'Parrot Bebop Drone':
#           item['label'] ==  'Parrot_Bebop_Drone'
#   json.dump(json_data, open(json_file).write(json_file), indent = 2)
# for json_file in os.listdir('.'):
#   open(json_file)
for json_file in os.listdir('.'):
    try:
        print('filename: ', json_file)
        json_data = json.load(open(json_file))
        print(json_data)
        for pred in json_data:
            if pred['label'] == 'AR Parrot Drone':
                pred['label'] = 'AR_Parrot_Drone'
            elif pred['label'] == 'Parrot Bebop Drone':
                pred['label'] = 'Parrot_Bebop_Drone'
        json.dump(json_data, open(json_file,'w'), indent = 2)
    except:
        continue