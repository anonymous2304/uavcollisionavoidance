# UAV Collision Avoidance Using an On-board Deep Learning Approach

This repository will allow you to bound and classify the Parrot Bebop 2 Drone, the Parrot AR 2.0 Elite Drone, and the DJI Mavic Air Drone on real time video feed by running the 'vision.py' 'script. The 'avoid.py' script lets a Parrot AR Drone take off and hover in place. Flying one of the three aforementioned drones towards the Parrot AR Drone used with the 'avoid.py' script will cause the AR to move out of the way to avoid collision.

![](./result_photos/air_detection.gif)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.


## Prerequisites
* A decent GPU
* Python3
* Cuda
* CudNN
* OpenCV (version 3 or higher)
* Tensorflow-GPU
* Numpy
* Matplotlib


Once you have the above things installed and working you will need to clone the pyardrone package with the following command:
```
git clone https://github.com/afq984/pyardrone.git
```

### Installing
First you will need unzip the 'ckpt.zip' folder in the uavreu repository.


Next, cd into the pyardrone directory and run the following command to install pyardrone.

```
pip3 install pyardrone
```

You can choose one of the three following ways to install Darfklow. I prefer the first option as it is the most simple. 


1. Just build the Cython extensions in place. NOTE: If installing this way you will have to use `./flow` in the cloned darkflow directory instead of `flow` as darkflow is not installed globally.
    ```
    python3 setup.py build_ext --inplace
    ```

2. Let pip install darkflow globally in dev mode (still globally accessible, but changes to the code immediately take effect)
    ```
    pip3 install -e .
    ```

3. Install with pip globally
    ```
    pip3 install .
    ```



## Running the model on a directory of images

To test the code on a directory of images of Parrot AR, Parrot Bebop 2, and/or the DJI Mavic Air drones run the following command.
  ```
    ./flow --imgdir ./drone_test --model cfg/tiny-yolo-voc-custom.cfg --load 67025 --gpu 1.0
  ```

 In this example, the image directory is called 'drone_test' and './flow' is used instead of 'flow'. If you installed darkflow with option 2 or 3, use 'flow'. If you are running out of memory, decrease the number after 'gpu' to a smaller number between 0 and 1.0.

### Running the model with live video feed in real time

If your PC already has a webcam, then you won't need to do anything for this step. Otherwise, hook up a webcam to your PC and run the following command.

```
python3 vision.py
```


  
If you don't have a Parrot AR, a Bebop, or a Mavic Air, you can try out the live video on a photo of one of the three. Even a photo pulled up on your phone should suffice. 

### Running the model on a prerecorded video 

  ```
    ./flow --demo prerecordedvideo.avi --model cfg/tiny-yolo-voc-custom.cfg --load 67025 --gpu 1.0
  ```


### Testing the collision avoidance script

To test the collision avoidance, you will need a Parrot AR 2.0 drone and either another Parrot AR Drone, a Parrot Bebop Drone, and/or a DJI Mavic Air Drone to test the code with. First, connect the computer you will be running the code on to the Parrot AR 2.0's wifi. Place the Parrot AR in a safe place, with plenty of room around it so that it doesn't damage any property. Before running the code, make sure that you have one of the drones that you will fly towards the AR for testing ready to fly. Once you have all of the above things ready, run the following code. Note: The collision avoidance algorithm is not 100% accurate, and its effectiveness will be partially dependent on the GPU that you are using. Use at your own risk.

```
python3 avoid.py
```


## Built With

* [Pyardrone](https://github.com/afq984/pyardrone) - Package for controlling the Parrot AR Drone
* [Darkflow](https://github.com/thtrieu) - Python-Tensorflow implementation of Darknet
